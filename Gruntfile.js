module.exports = function(grunt) {
	// Project configuration.
	grunt.initConfig({
		cssmin: {
			options: {
				shorthandCompacting: false,
				roundingPrecision: -1
			},
			main: {
				files: {
					'app/dist/style.min.css': ['app/css/style.css']
				}
			}
		},
		concat: {
			// concat task configuration goes here.
			main: {
				options: {
					separator: ';',
					stripBanners: true,
				},
				src: ['app/app.js', 'app/**/*.js', '!app/**/*.spec.js', '!app/dist/**/*.js'],
				dest: 'app/dist/main.js',
			},
			dependenciesCss: {
				src: [
					'node_modules/angular-material/angular-material.min.css',
					'node_modules/angular-toastr/dist/angular-toastr.min.css',
					'node_modules/angular-ui-bootstrap/dist/ui-bootstrap-csp.css',
					'node_modules/bootstrap/dist/css/bootstrap.min.css',
				],
				dest: 'app/dist/dependencies.min.css'
			},
			dependenciesJs: {
				src: [
					'node_modules/angular/angular.min.js',
					'node_modules/angular-animate/angular-animate.min.js',
					'node_modules/angular-aria/angular-aria.min.js',
					'node_modules/angular-cookies/angular-cookies.min.js',
					'node_modules/angular-file-upload/dist/angular-file-upload.min.js',
					'node_modules/angular-material/angular-material.min.js',
					'node_modules/angular-toastr/dist/angular-toastr.min.js',
					'node_modules/angular-toastr/dist/angular-toastr.tpls.min.js',
					'node_modules/angular-ui-bootstrap/dist/ui-bootstrap-tpls.js',
					'node_modules/angular-ui-router/release/angular-ui-router.min.js',
					'node_modules/angular-websocket/dist/angular-websocket.min.js',
					'node_modules/jquery/dist/jquery.min.js',
					'node_modules/bootstrap/dist/js/bootstrap.min.js',
					'node_modules/moment/min/moment.min.js',
					'node_modules/underscore/underscore-min.js',
				],
				dest: 'app/dist/dependencies.min.js',
			}
		},
		uglify: {
			main: {
				options: {
					mangle: false,
					preserveComments: false
				},
				src: ['app/app.js', 'app/js/**/*.js', '!app/**/*.spec.js', '!app/dist/**/*.js'],
				dest: 'app/dist/main.min.js',
			}
		},
		copy: {
			jsmap: {
				files: [
					{
						expand: true,
						flatten: true,
						src: [
							'node_modules/angular/angular.min.js.map',
							'node_modules/angular-animate/angular-animate.min.js.map',
							'node_modules/angular-aria/angular-aria.min.js.map',
							'node_modules/angular-cookies/angular-cookies.min.js.map',
							'node_modules/angular-file-upload/dist/angular-file-upload.min.js.map',
							'node_modules/angular-ui-router/release/angular-ui-router.min.js.map',
							'node_modules/angular-websocket/dist/angular-websocket.min.js.map',
							'node_modules/jquery/dist/jquery.min.map',
							'node_modules/underscore/underscore-min.map',
						],
						dest: 'app/dist/',
						filter: 'isFile'
					},
				],
			},
			cssmap: {
				files: [
					{
						expand: true,
						flatten: true,
						src: [
							//'node_modules/bootstrap/dist/css/bootstrap.min.css.map',
						],
						dest: 'app/dist/',
						filter: 'isFile'
					},
				],
			}
		},
		watch: {
			//run unit tests with karma (server needs to be already running)
			testTask: {
				files: [
					'app/app.js',
					'app/module.js',
					'app/**/*.css',
					'app/**/*.html',
					'app/**/*.js',
					'app/**/*.spec.js',
					'!app/dist/**',
					'!**/node_modules/**'
				],
				tasks: ['dist']
			},
		},
		ngtemplates: {
			main: {
				src: ['app/**/*.html'],
				dest: 'app/dist/templates.js',
				options: {
					module: 'wxweb.templates',
					standalone: true,
					htmlmin: {
						collapseBooleanAttributes: true,
						collapseWhitespace: true,
						removeAttributeQuotes: true,
						removeComments: true, // Only if you don't use comment directives! 
						removeEmptyAttributes: true,
						removeRedundantAttributes: true,
						removeScriptTypeAttributes: true,
						removeStyleLinkTypeAttributes: true
					},
					url: function(url) {
						return url.replace('app/', '');
					}
				}
			}
		},
		eslint: {
			options: {
				quiet: true
			},
			target: ['app/app.js', 'app/js/**.js']
		},
		karma: {
			options: {
				configFile: 'karma.conf.js',
			},
			start: {
				configFile: 'karma.conf.js',
			},
			unit: {
				configFile: 'karma.conf.js'
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-angular-templates');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-karma');
	grunt.loadNpmTasks('grunt-eslint');

	//tasks
	grunt.registerTask('default', ['cssmin', 'concat', 'uglify', 'ngtemplates', 'copy', 'watch']);
	grunt.registerTask('dist', ['cssmin', 'concat', 'uglify', 'ngtemplates']);
	grunt.registerTask('test', ['karma:start', 'watch']);
};