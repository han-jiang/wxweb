// http://wxsrv.han.pm:4454
var base_url = "http://wxsrv.han.pm:4454";

(function() {
	'use strict';

	angular.module('wxWeb', ['wxweb.templates', 'ui.router', 'ngCookies', 'ngMaterial', 'ngWebSocket', 'toastr', 'angularFileUpload'])
		.config(['$stateProvider', 'toastrConfig', wxWebConfig])
		.run(wxWebRun);

	function wxWebConfig($stateProvider, toastrConfig) {
		$stateProvider.state({
			name: 'wxweb',
			url: '/',
			templateUrl: 'js/webwx/webWeiXin.html',
			controller: 'WebWeiXinLoginController',
			controllerAs: 'vm'
		}).state({
			name: 'devices',
			url: '/devices',
			templateUrl: 'js/device/devices.html',
			controller: 'DevicesController',
			controllerAs: 'vm'
		}).state({
			name: 'accountId',
			url: '/account/:accountId',
			templateUrl: 'js/account/account.html',
			controller: 'AccountController',
			controllerAs: 'vm'
		}).state({
			name: 'friends',
			url: '/friends',
			templateUrl: 'js/friends/friends.html',
			controller: 'FriendsController',
			controllerAs: 'vm'
		}).state({
			name: 'groups',
			url: '/groups',
			templateUrl: 'js/groups/groups.html',
			controller: 'GroupsController',
			controllerAs: 'vm'
		}).state({
			name: 'groupId',
			url: '/group/:groupId',
			templateUrl: 'js/group/group.html',
			controller: 'GroupController',
			controllerAs: 'vm'
		}).state({
			name: 'groupIntro',
			url: '/group-intro',
			templateUrl: 'js/groupIntro/groupIntro.html',
			controller: 'GroupIntroController',
			controllerAs: 'vm'
		}).state({
			name: 'groupIntroId',
			url: '/group-intro/:groupId',
			templateUrl: 'js/group/group.html',
			controller: 'GroupController',
			controllerAs: 'vm'
		}).state({
			name: 'groupSend',
			url: '/group-send',
			templateUrl: 'js/groupSend/groupSend.html',
			controller: 'GroupSendController',
			controllerAs: 'vm'
		}).state({
			name: 'batchGroupSend',
			url: '/batch-group-send',
			templateUrl: 'js/batchSend/batchGroupSend.html',
			controller: 'BatchGroupSendController',
			controllerAs: 'vm'
		}).state({
			name: 'groupForward',
			url: '/group-forward',
			templateUrl: 'js/groupForward/groupForward.html',
			controller: 'GroupForwardController',
			controllerAs: 'vm'
		}).state({
			name: 'groupInvite',
			url: '/group-invite',
			templateUrl: 'js/groupInvite/groupInvite.html',
			controller: 'GruntInviteController',
			controllerAs: 'vm'
		}).state({
			name: 'autoReply',
			url: '/auto-reply',
			templateUrl: 'js/autoReply/autoReply.html',
			controller: 'AutoReplyController',
			controllerAs: 'vm'
		}).state({
			name: 'login',
			url: '/login',
			templateUrl: 'js/auth/login.html',
			controller: 'LoginController',
			controllerAs: 'vm'
		}).state({
			name: 'logout',
			url: '/logout',
			templateUrl: 'js/auth/login.html',
			controller: 'LoginController',
			controllerAs: 'vm'
		}).state({
			name: 'upload',
			url: '/upload',
			templateUrl: 'js/media/upload.html',
			controller: 'UploadController',
			controllerAs: 'vm'
		}).state({
			name: 'media',
			url: '/media',
			templateUrl: 'js/media/media.html',
			controller: 'MediaController',
			controllerAs: 'vm'
		}).state({
			name: 'about',
			url: '/about',
			templateUrl: 'js/about/about.html'
		}).state({
			name: 'contact',
			url: '/contact',
			templateUrl: 'js/about/contact.html'
		}).state({
			name: 'selfIntro',
			url: '/self-intro',
			templateUrl: 'js/selfIntro/selfIntro.html',
			controller: 'SelfIntroController',
			controllerAs: 'vm'
		}).state({
			name: 'chat',
			url: '/chat?wxid=?nickName=?avatar=',
			templateUrl: 'js/chat/chat.html',
			controller: 'ChatController',
			controllerAs: 'vm'
		});

		angular.extend(toastrConfig, {
			allowHtml: false,
			closeButton: true,
			closeHtml: '<button>&times;</button>',
			extendedTimeOut: 1000,
			iconClasses: {
				error: 'toast-error',
				info: 'toast-info',
				success: 'toast-success',
				warning: 'toast-warning'
			},
			messageClass: 'toast-message',
			onHidden: null,
			onShown: null,
			onTap: null,
			progressBar: true,
			tapToDismiss: true,
			timeOut: 1500,
			titleClass: 'toast-title',
			toastClass: 'toast'
		});
		//注入ToastrAlert之后将下面这句放到需要用到的地方
		//ToastrAlert.sendStatus('success', 'message', 'title', 500);
		//状态有success, info, warning, 和error
		//第二个是参数是message(必须),
		//第三个参数是title(optional),不填则不显示title
		//第四个参数是timeout, 用ms数值, 例如500, 不填则默认1000
	}

	function wxWebRun($rootScope, $location, $cookies) {
		$rootScope.$on('$stateChangeStart', function(event, next) {
			if (!$cookies.get("userId")) {
				if (next.templateUrl == "html/login.html") {
					// already going to #login
				} else if (next.templateUrl == "html/about.html") {

				} else if (next.templateUrl == "html/contact.html") {
					// going to #contact, show contact page, no redirect needed
				} else {
					// going to others, we should redirect to #login page now
					$state.go("login");
				}
			}
		});
	}
})();