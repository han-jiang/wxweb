(function() {
	"use strict";

	angular.module('wxWeb').controller('LoginController', ['$rootScope', '$location', '$cookieStore', 'User', 'LoginService', 'ToastrAlert', LoginController]);

	function LoginController($rootScope, $location, $cookieStore, User, LoginService, ToastrAlert) {
		var vm = this;

		vm.users = [];
		vm.showLoginDialog = false;

		vm.loginData = {
			userName: '',
			password: ''
		};

		vm.hideLoginDialog = function() {
			$('#myLoginModal').modal('hide');
		};

		vm.login = function() {
			var password = {
				password: vm.loginData.password
			};

			LoginService.login(vm.loginData).then(function(response) {
				if (response.data.data == "ok") {
					User.setUserId(vm.loginData.userName);
					$location.path("/about");
					ToastrAlert.sendStatus("success", "登陆成功", 1000);
				}
			}, function(error) {
				ToastrAlert.sendStatus("error", "请检查网络连接", "登陆失败", 1000);
			});
		};

		vm.loginVerify = function() {
			for (var i = 0; i < vm.users.length; i++) {
				if (vm.loginData.userName == vm.users[i].userName && vm.loginData.password == vm.users[i].password) {
					ToastrAlert.sendStatus("success", "登陆成功", 500);
				}
			}
		};

		vm.loginSuccess = function() {
			ToastrAlert.sendStatus("success", "登陆成功", 500);
		};

		vm.isUserLogin = function() {
			return User.isLogin();
		};
	}
})();