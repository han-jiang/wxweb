(function() {
	"use strict";

	angular.module('wxWeb').service('LoginService', ['$http', 'User', LoginService]);

	function LoginService($http, User) {
		var svc = this;

		svc.login = function(loginData){
			return $http({
				method: 'POST',
				url: base_url + '/version/1.0/login/' + loginData.userName,
				data: {
					password: loginData.password
				},
				withCredentials: true
			});
		};

		svc.logout = function() {
			return $http({
				method: 'GET',
				url: base_url + '/logout',
				withCredentials: true
			});
		};
	}
})();