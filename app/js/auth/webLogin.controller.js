(function() {
	angular.module('wxWeb').controller('WebWeiXinLoginController', ['User', 'WebWeiXinLoginService', WebWeiXinLoginController]);

	function WebWeiXinLoginController(User, WebWeiXinLoginService) {
		var vm = this;

		vm.login = function() {
			if (!vm.qrurl || vm.qrurl.length === 0) return;

			var qrurl = vm.qrurl.replace("qrcode", "l");

			WebWeiXinLoginService.login(User.currentDeivce).then(function(response) {}, function(error) {});
		};

		vm.isLogin = function() {
			return User.isLogin();
		};
	}
})();