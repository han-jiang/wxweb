(function() {
	"use strict";
	angular.module('wxWeb').service('WebWeiXinLoginService', ['$http', WebWeiXinLoginService]);

	function WebWeiXinLoginService() {
		var svc = this;
		svc.login = function(deviceId) {
			return $http({
				method: 'POST',
				url: base_url + '/device/' + deviceId + '/weblogin/',
				data: {
					data: qrurl
				},
				withCredentials: true
			});
		};
	}
})();