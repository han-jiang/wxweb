(function() {
	"use strict";

	angular.module('wxWeb').controller('BatchGroupSendController', ['$scope', '$fileUploader', '$timeout', 'User', 'BatchGroupSendService', 'ToastrAlert', BatchGroupSendController]);

	function BatchGroupSendController($scope, $fileUploader, $timeout, User, BatchGroupSendService, ToastrAlert) {

		var vm = this;

		vm.formData = {};
		vm.myreceiver = User.userId;
		vm.item = {};
		vm.deviceName = User.deviceName;
		vm.delayTime = 3;
		vm.intervalTime = 20;

		vm.ref = new Wilddog("https://wxweb.wilddogio.com/" + User.userId);

		// 监听数据
		vm.ref.on("value", function(snapshot) {
			$timeout(function() {
				if (snapshot !== null) {
					vm.myreceiver = snapshot.val().receiver;
					vm.wild_myreceiver = snapshot.val().receiver;
				}
			});
		}, function(errorObject) {
			console.log("The read failed: " + errorObject.code);
		});


		vm.setReceiver = function() {
			vm.item.$editing = false;
			var key = vm.data;
			vm.ref.set({
				receiver: vm.myreceiver
			});
		};

		vm.sendMsg = function(content, msgType, countdown) {
			var data = {
				content: content,
				messageType: msgType,
				countDown: countdown,
				receiver: vm.myreceiver,
				currentDeviceId: User.currentDeviceId
			};
			BatchGroupSendService.sendMessage(data).then(function(data) {
				ToastrAlert.sendStatus("success", content, "发送成功", 1000);
			}, function(data) {
				ToastrAlert.sendStatus("error", data, "发送失败", 1000);
			});
		};

		// process the form
		vm.sendAll = function(messages) {
			vm.ref.set({
				receiver: vm.myreceiver
			});

			for (var item in messages) {
				vm.sendMsg(
					messages[item].postContent,
					messages[item].msgType,
					messages[item].countdown);
			}
		};

		vm.preProcess = function() {
			var messages = vm.formData.data.split("\n");
			var startTime = vm.delayTime;
			var msgType = "text";

			for (var item in messages) {
				if (messages[item].substr(-4) == ".amr") {
					msgType = "audio";
				} else if (messages[item].substr(-4) == ".jpg" || messages[item].substr(-4) == ".png") {
					msgType = "image";
				} else {
					msgType = "text";
				}

				var msg = {
					postContent: messages[item],
					msgType: msgType,
					countdown: startTime
				};
				messages.push(msg);
				startTime = startTime + vm.intervalTime;
			}
			vm.sendAll(messages);
		};
	}
})();