(function() {
	"use strict";

	angular.module('wxWeb').service('BatchGroupSendService', ['$http', BatchGroupSendService]);


	function BatchGroupSendService($http) {
		var svc = this;
		svc.sendMessage = function(msgData) {
			return $http({
				method: 'POST',
				url: base_url + '/session/' + msgData.receiver + '/bydevice/' + msgData.currentDevice + '/' + msgData.messageType,
				data: {
					data: msgData.content,
					countdown: msgData.countDown
				},
				withCredentials: true
			});
		};
	}
})();