(function() {
	'use strict';

	angular.module('wxWeb').controller('ChatController', ['$scope', '$state', '$stateParams', 'ChatService', 'User', ChatController]);

	function ChatController($scope, $state, $stateParams, chatService, User) {
		var vm = this;
		console.log($stateParams.img2, '$stateParams.img2');
		vm.toFriend = {
			wxid: $stateParams.wxid,
			nickName: $stateParams.nickName,
			avatar: $stateParams.avatar
		};

		vm.inputMessage = {
			fromMe: true,
			content: '',
			avatar: 'https://pic4.zhimg.com/26007402ddf17954b86cd2a90997dfdb_xl.png'
		};

		vm.messages = [];

		vm.processMessage = function($event) {
			if ((!$event || $event && $event.keyCode == 13) && vm.inputMessage.content.length > 0) {
				if ($event.altKey) {
					vm.inputMessage.content += '\n';
				} else {
					sendMessage();
				}
			}
		};

		function sendMessage() {
			vm.messages.push(angular.copy(vm.inputMessage));
			chatService.send({
				from: User.userId,
				to: vm.toFriend.wxid,
				message: vm.inputMessage.content,
				time: Date.now()
			});

			vm.inputMessage.content = '';
		}

		$scope.$watch(function() {
			return chatService.collection.length;
		}, function(newValue, oldValue) {
			if (newValue) {
				vm.messages.push({
					fromMe: false,
					content: chatService.collection[newValue - 1].replace(/\n/g, '<br />'),
				});
			}
		});

		vm.init = function() {};

		vm.init();
	}
})();