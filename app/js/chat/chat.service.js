(function() {
	'use strict';

	angular.module('wxWeb').service('ChatService', function($websocket) {
		var dataStream = $websocket('ws://localhost:9292');
		var collection = [];

		dataStream.onMessage(function(message) {
			collection.push(message.data);
		});

		return {
			collection: collection,
			send: function(message) {
				dataStream.send(JSON.stringify(message));
			}
		};
	});
})();