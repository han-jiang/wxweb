(function() {
	"use strict";

	angular.module('wxWeb').controller('DevicesController', ['User', 'DeviceService', 'ToastrAlert', DevicesController]);

	function DevicesController(User, DeviceService, ToastrAlert) {
		var vm = this;
		vm.user = User;
		vm.isDevicesLoaded = false;

		vm.chooseDevice = function(device) {
			vm.devices.forEach(function(d) {
				d.status = undefined;
			});

			device.status = "已选择";
			User.chooseDevice(device.wxid);
		};

		vm.init = function() {
			vm.isDevicesLoaded = false;
			DeviceService.getDevices().then(function(response) {
				vm.devices = response.data.devices;
				updateDeviceStatus(vm.devices);
				vm.isDevicesLoaded = true;
			}, function() {
				ToastrAlert.sendStatus("error", "加载设备列表失败", 1000);
			});
		};

		function updateDeviceStatus(devices) {
			devices.forEach(function(d) {
				if (d.wxid == User.currentDeviceId)
					d.status = "已选择";
			});
		}

		vm.init();
	}
})();