(function() {
	"use strict";
	angular.module('wxWeb').service('DeviceService', ['$http', DeviceService]);

	function DeviceService($http) {
		var svc = this;
		svc.getDevices = function() {
			return $http({
				method: 'GET',
				url: base_url + '/device/',
				withCredentials: true
			});
		};
	}
})();