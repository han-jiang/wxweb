(function() {
	"use strict";
	angular.module('wxWeb').controller('FriendsController', ['$state', 'User', 'FriendsService', 'ToastrAlert', FriendsController]);

	function FriendsController($state, User, FriendsService, ToastrAlert) {
		var vm = this;

		vm.items = [];
		vm.page = {
			size: 10,
			index: 1
		};
		vm.isFriendsLoaded = false;

		vm.sort = {
			column: 'wxid',
			direction: -1,
			toggle: function(column) {
				if (column.sortable === false)
					return;

				if (this.column === column.name) {
					this.direction = -this.direction || -1;
				} else {
					this.column = column.name;
					this.direction = -1;
				}
			}
		};

		vm.checkAll = function(checked) {
			angular.forEach(vm.items, function(item) {
				item.$checked = checked;
			});
		};

		vm.selection = function() {
			return vm.items.filter(function(item) {
				return item.$checked;
			});
		};

		vm.getFriends = function() {
			if (!User.currentDeviceId) {
				ToastrAlert.sendStatus('error', '请先选择设备', 1000);
				return;
			}

			vm.isFriendsLoaded = false;
			FriendsService.getFriendsListByDevice(User.currentDeviceId).then(function(resp) {
				vm.items = resp.data.data;
				vm.page.size = vm.items.length;
				vm.isFriendsLoaded = true;
			}, function(error) {
				ToastrAlert.sendStatus('error', '加载好友列表失败', 500);
			});
		};

		vm.chatWithFriend = function(item) {
			$state.go('chat', {
				wxid: item.wxid,
				nickName: item.nickName,
				avatar: item.img2
			});
		};

		vm.init = function() {
			vm.getFriends();
		};

		vm.init();
	}
})();