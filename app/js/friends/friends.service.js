(function() {
	"use strict";
	angular.module("wxWeb").service('FriendsService', ['$http', FriendsService]);

	function FriendsService($http) {
		var svc = this;

		svc.getFriendsListByDevice = function(device) {
			return $http({
				method: 'GET',
				url: base_url + '/friend/bydevice/' + device + '/',
				withCredentials: true,
				cache: true
			});
		};
	}
})();