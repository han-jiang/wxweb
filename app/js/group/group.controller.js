(function() {
	"use strict";

	angular.module('wxWeb').controller('GroupController', ['$stateParams', '$location', '$timeout', 'User', 'GroupService', 'ToastrAlert', GroupController]);

	function GroupController($stateParams, $location, $timeout, User, GroupService, ToastrAlert) {

		var vm = this;

		vm.intro = "default";
		vm.wxid = $stateParams.groupId;
		vm.groupInfo = {};

		vm.getIntro = function(wxid) {
			GroupService.getIntro(vm.wxid).then(function(response) {
				vm.intro = response.data.content;
				vm.name = response.data.name;
				vm.time = response.data.time;
			}, function(error) {

			});
		};

		//http://linode.han.pm:4454/group/1740097255@chatroom/bydevice/wxid_mjwgzjutap6912/
		vm.getGroupInfo = function(wxid) {
			GroupService.getGroupIntro(vm.wxid).then(function(response) {
				vm.groupInfo = response.data.data;
			}, function(error) {

			});
		};

		vm.setIntro = function() {
			vm.editing = false;
			GroupService.setIntro(vm.wxid).then(function(data) {
				ToastrAlert.sendStatus("success", "群介修改绍成功！", 1000);
			}, function(data) {
				ToastrAlert.sendStatus("error", "群介修改绍失败！", 1000);
			});
		};

		vm.getIntro($stateParams.groupId);
		vm.getGroupInfo($stateParams.groupId);
	}
})();