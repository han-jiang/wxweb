(function() {
	"use strict";

	angular.module('wxWeb').service('GroupService', ['$http', 'User', GroupService]);

	function GroupService($http, User) {
		var svc = this;
		svc.getIntro = function(wxid) {
			return $http({
				method: 'GET',
				url: base_url + '/group/' + wxid + '/intro/',
				withCredentials: true
			});
		};
		svc.getGroupIntro = function(wxid) {
			return $http({
				method: 'GET',
				url: base_url + '/group/' + wxid + '/bydevice/' + User.currentDevice + '/',
				withCredentials: true
			});
		};

		svc.setIntro = function(wxid) {
			return $http({
				method: 'POST',
				url: base_url + '/group/' + wxid + '/intro/',
				data: {
					content: vm.intro,
					time: vm.time
				},
				withCredentials: true
			});
		};
	}
})();