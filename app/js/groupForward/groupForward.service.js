(function() {
	"use strict";

	angular.module('wxWeb').service('GroupForwardService', ['$http', GroupForwardService]);

	function GroupForwardService($http) {
		var svc = this;

		svc.getNameGroup = function(deviceId) {
			return $http({
				method: 'GET',
				url: base_url + '/json/' + deviceId + '/namedgroup.json',
				withCredentials: true
			});
		};

		svc.setNameGroup = function(deviceId, items){
			return $http({
				method: 'POST',
				url: base_url + '/json/' + deviceId + '/namedgroup.json',
				data: items,
				withCredentials: true
			});
		};
	}
})();