(function() {
	"use strict";

	angular.module('wxWeb').controller('GroupForwardController', ['User', 'GroupForwardService', 'ToastrAlert', GroupForwardController]);

	function GroupForwardController(User, GroupForwardService, ToastrAlert) {
		var vm = this;

		vm.getNameGroup = function() {
			GroupForwardService.getNameGroup(User.currentDeviceId).then(function(response) {
				vm.items = response.data.data;
			}, function(data) {
				ToastrAlert.sendStatus("error", "获取群组失败！", 1000);
			});
		};

		vm.setNameGroup = function() {
			GroupForwardService.setNameGroup(User.currentDeviceId, vm.items).then(function(res) {
				ToastrAlert.sendStatus("success", "群转发保存成功", 1000);
			}, function(err) {
				ToastrAlert.sendStatus("error", "群转发保存失败", 1000);
			});
		};

		vm.addItem = function() {
			var item = {
				key: vm.formData.keyword,
				value: vm.formData.roomId
			};
			vm.formData.keyword = null;
			vm.formData.roomId = null;

			vm.items.push(item);
		};

		vm.delItem = function(index) {
			vm.items.splice(index, 1);
		};

		vm.getItemsJson = function() {
			var itemsJson = {};
			for (var item in vm.items) {
				itemsJson[vm.items[item].key] = vm.items[item].value;
			}
			return itemsJson;
		};

		vm.init = function() {
			vm.getNameGroup();
		};

		vm.init();
	}
})();