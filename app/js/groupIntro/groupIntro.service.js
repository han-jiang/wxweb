(function() {
	"use strict";

	angular.module('wxWeb').service('GroupIntroService', ['$http', GroupIntroService]);

	function GroupIntroService($http) {
		var svc = this;

		svc.getIntroById = function(wxid) {
			return $http({
				method: 'GET',
				url: base_url + '/group/' + wxid + '/intro/',
				withCredentials: true
			});
		};

		svc.getAllIntros = function() {
			return $http({
				method: 'GET',
				url: base_url + '/group/byhasintro/?filter=大佬招聘',
				withCredentials: true,
				cache: true
			});
		};

		svc.setIntro = function(wxid, introData) {
			return $http({
				method: 'POST',
				url: base_url + '/group/' + wxid + '/intro/',
				data: {
					data: introData.intro,
					time: introData.time
				},
				withCredentials: true
			});
		};
	}
})();