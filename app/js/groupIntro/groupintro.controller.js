(function() {
	"use strict";
	angular.module('wxWeb').controller('GroupIntroController', ['$stateParams', '$location', 'GroupIntroService', 'ToastrAlert', GroupIntroController]);

	function GroupIntroController($stateParams, $location, GroupIntroService, ToastrAlert) {

		var vm = this;
		vm.intro = "default";

		vm.getIntro = function(wxid) {
			GroupIntroService.getIntroById(wxid).then(function(response) {
				vm.intro = response.data.content;
				vm.name = response.data.name;
				vm.time = response.data.time;
			}, function(error) {});
		};

		vm.getAllIntros = function() {
			//Why base_url+ '/group/byhasintro/?filter=大佬招聘' ？？？
			GroupIntroService.getAllIntros().then(function(response) {
				vm.items = response.data.data;
			}, function(error) {
				ToastrAlert.sendStatus("error", "加载群介绍失败", 1000);
			});
		};

		vm.setIntro = function() {
			vm.editing = false;
			var introData = {
				intro: vm.intro,
				time: vm.time
			};

			GroupIntroService.setIntro(vm.wxid, introData).then(function(response) {
				ToastrAlert.sendStatus("success", "群介绍修改成功", 1000);
			}, function(error) {
				ToastrAlert.sendStatus("error", "群介绍修改失败", 1000);
			});
		};

		vm.page = {
			size: 10,
			index: 1
		};

		vm.sort = {
			column: 'key',
			direction: -1,
			toggle: function(column) {
				if (column.sortable === false)
					return;

				if (this.column === column.name) {
					this.direction = -this.direction || -1;
				} else {
					this.column = column.name;
					this.direction = -1;
				}
			}
		};
		// 构建模拟数据
		vm.columns = [{
				label: '群名',
				name: 'key',
				type: 'string'
			}, {
				label: '群介绍',
				name: 'value',
				type: 'string',
				sortable: false
			}, {
				label: '操作',
				name: 'roomowner',
				sortable: false
			}
		];

		// 判断是单独还是获取所有群介绍消息
		if ($location.path() == "/group-intro") {
			vm.getAllIntros();
		} else {
			vm.wxid = $stateParams.groupId;
			vm.getIntro($stateParams.groupId);
		}
	}
})();