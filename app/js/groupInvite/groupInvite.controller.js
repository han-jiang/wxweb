(function() {
	"use strict";
	angular.module('wxWeb').controller('GruntInviteController', ['$location', 'User', 'GroupInviteService', 'ToastrAlert', GruntInviteController]);

	function GruntInviteController($location, User, ItemsService, ToastrAlert) {
		var vm = this;

		vm.clicked = false;
		vm.items = [];

		if ($location.path() == "/group-forward") {
			vm.title = "转发设置";
		} else if ($location.path() == "/group-invite") {
			vm.title = "自动邀请入群设置";
			console.log(vm.title);
		}

		vm.getItems = function() {
			ItemsService.getItems().then(function(response) {
				if (response.data.data)
					vm.items = response.data.data;
			}, function() {
				ToastrAlert.sendStatus("error", "获取群组失败", 1000);
			});
		};

		vm.setItems = function() {
			ItemsService.setItems(vm.items).then(function(data) {
				vm.clicked = false;
				ToastrAlert.sendStatus("success", "设置保存失败", 1000);
			}, function(error) {
				ToastrAlert.sendStatus("error", "设置保存失败", 1000);
			});
		};

		vm.addItem = function() {
			if (angular.isDefined(vm.formData)) {
				var item = {
					key: vm.formData.keyword,
					value: vm.formData.roomId
				};
				vm.formData.keyword = null;
				vm.formData.roomId = null;

				vm.items.push(item);
			}
		};

		vm.addAndSetItems = function() {
			vm.addItem();
			vm.setItems();
		};

		vm.delItem = function(index) {
			vm.items.splice(index, 1);
		};

		vm.getItems();
	}
})();