(function() {
	"use strict";

	angular.module('wxWeb').service('GroupInviteService', ['$http', '$location', 'User', GroupInviteService]);

	function GroupInviteService($http, $location, User) {
		var svc = this;

		var itemsUrl;

		if ($location.path() == "/group-forward") {
			itemsUrl = base_url + "/json/dalao/repost.json";
		} else if ($location.path() == "/group-invite") {
			itemsUrl = base_url + '/json/' + User.currentDeviceId + '/group_invite.json';
		}

		svc.getItems = function() {
			return $http({
				method: 'GET',
				url: itemsUrl,
				withCredentials: true
			});
		};

		svc.setItems = function(items) {
			return $http({
				method: 'POST',
				url: itemsUrl,
				data: items,
				withCredentials: true
			});
		};
	}
})();