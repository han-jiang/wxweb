(function() {
	"use strict";

	angular.module('wxWeb').service('GroupSendService', ['$http', GroupSendService]);

	function GroupSendService($http) {
		var svc = this;

		svc.processForm = function(formData, deviceId, receiver, type) {
			return $http({
				method: 'POST',
				url: base_url + '/session/' + receiver + '/bydevice/' + deviceId + '/' + type,
				data: formData, // pass in data as strings
				withCredentials: true
				// headers : { 'Content-Type': 'application/x-www-form-urlencoded' }  // set the headers so angular passing info as form data (not request payload)
			});
		};
	}
})();