(function() {

	"use strict";

	angular.module('wxWeb').controller('GroupSendController', ['$timeout', 'FileUploader', 'User', 'GroupSendService', 'ToastrAlert', GroupSendController]);

	function GroupSendController($timeout, FileUploader, User, GroupSendService, ToastrAlert) {

		var vm = this;
		//console.log(User);
		// $scope.setName = function() {
		//    Data.name = "Jack";
		//  }

		vm.cities = [{
				code: 'group',
				label: '所有群'
			}, {
				code: 'friend',
				label: '所有好友'
			}, {
				code: 'all',
				label: '所有群与好友'
			}
		];

		vm.formData = {};
		vm.myreceiver = User.userId;
		vm.item = {};
		vm.deviceName = User.device_name;

		vm.ref = new Wilddog("https://wxweb.wilddogio.com/" + User.userId);
		//console.log(vm.ref, 'ref');

		// 监听数据
		vm.ref.on("value", function(snapshot) {
			// console.log("一开始就被运行了~");
			// console.log(snapshot.val());

			if (!snapshot) return;

			$timeout(function() {
				if (snapshot !== null) {
					vm.myreceiver = snapshot.val().receiver;
					vm.wild_myreceiver = snapshot.val().receiver;
				}
				// $scope.time = new Date();
			});

			// vm.myreceiver = snapshot.val().list;
			// console.log(vm.myreceiver);

		}, function(errorObject) {
			//console.log("The read failed: " + errorObject.code);
		});

		// ref.set({
		// 	name : "jianghan9013",
		// 	text : "hello@chatroom"
		// });

		vm.setReceiver = function() {
			vm.item.$editing = false;
			var key = vm.data;
			vm.ref.set({
				receiver: vm.myreceiver
			});
		};

		// process the form
		vm.processForm = function(type) {

			vm.ref.set({
				receiver: vm.myreceiver
			});

			if (vm.delaytime > 0) {
				vm.formData.countdown = vm.delaytime;
			}

			GroupSendService.processForm(vm.formData, User.currentDeviceId, vm.myreceiver, type).then(function(response) {
				// if successful, bind success message to message and alert
				ToastrAlert.sendStatus('success', vm.formData.data, '消息发送成功', 1000);
				vm.message = response.data.message;
			}, function() {
				// if not successful, bind errors to error variables
				ToastrAlert.sendStatus("error", "消息发送失败", 1000);
				// $scope.errorName = data.errors.name;
				// $scope.errorSuperhero = data.errors.superheroAlias;
			});
		};

		vm.selectAction = function() {
			if (!vm.value) {
				vm.myreceiver = vm.wild_myreceiver;
			} else {
				vm.myreceiver = vm.value.code;
			}
		};

		vm.imageUploader = new FileUploader({
			url: 'upload.html'
		});
	}
})();