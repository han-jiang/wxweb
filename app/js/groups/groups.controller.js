(function() {
	"use strict";
	angular.module('wxWeb').controller('GroupsController', ['User', 'GroupsService', 'ToastrAlert', GroupsController]);

	function GroupsController(User, GroupsService, ToastrAlert) {
		var vm = this;

		vm.page = {
			size: 10,
			index: 1
		};
		vm.groupsList = [];

		vm.sort = {
			column: 'wxid',
			direction: -1,
			toggle: function(column) {
				if (column.sortable === false)
					return;

				if (this.column === column.name) {
					this.direction = -this.direction || -1;
				} else {
					this.column = column.name;
					this.direction = -1;
				}
			}
		};
		// 构建模拟数据
		vm.columns = [{
				label: 'wxid',
				name: 'wxid',
				type: 'string'
			}, {
				label: '群名',
				name: 'name',
				type: 'string'
			}, {
				label: '群成员数',
				name: 'count',
				type: 'number'
			}, {
				label: '群主',
				name: 'roomowner',
				type: 'string'
			}, {
				label: '',
				name: 'actions',
				sortable: false
			}
		];

		vm.checkAll = function(checked) {
			angular.forEach(vm.groupsList, function(item) {
				item.$checked = checked;
			});
		};

		vm.selection = function() {
			var selected = _.where(vm.groupsList, {
				$checked: true
			});
			return selected;
		};

		GroupsService.getGroupsList().then(function(response) {
			vm.groupsList = response.data.data;
		}, function(error) {
			ToastrAlert.sendStatus("error", "获取群组列表失败", "非常抱歉", 1000);
		});
	}
})();