(function() {
	"use strict";

	angular.module('wxWeb').service('GroupsService', ['$http', 'User', GroupsService]);

	function GroupsService($http, User) {
		var svc = this;
		svc.getGroupsList = function() {
			return $http({
				method: 'GET',
				url: base_url + '/group/bydevice/' + User.currentDeviceId + '/',
				withCredentials: true,
				cache: true
			});
		};
	}
})();