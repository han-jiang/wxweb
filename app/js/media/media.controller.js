(function() {
	"use strict";

	angular.module('wxWeb').controller('MediaController', MediaController);

	function MediaController() {
		var vm = this;

		var TestObject = AV.Object.extend('TestObject');
		var testObject = new TestObject();
		testObject.save({
			words: 'Hello World!'
		}, {
			success: function(object) {
				console.log('LeanCloud Rocks!');
			}
		});

	}
})();