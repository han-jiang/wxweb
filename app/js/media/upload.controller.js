(function() {
	"use strict";

	angular.module('wxWeb').controller('UploadController', ['$scope', 'FileUploader', 'ToastrAlert', UploadController]);

	function UploadController($scope, FileUploader, ToastrAlert) {
		var vm = this;
		vm.uploadedFileTotalNum = 0;

		var uploader = $scope.uploader = new FileUploader({
			url: 'upload.php'
		});

		vm.uploadFileItem = function(queueItem) {
			if (!queueItem.isReady) {
				var avFile = new AV.File(queueItem._file.name, queueItem._file);
				avFile.save().then(function(obj) {
					console.log(obj.url());
					vm.uploadedFileTotalNum++;

					uploader.progress = (vm.uploadedFileTotalNum / uploader.queue.length).toFixed(2) * 100;
					queueItem.isReady = true;

					$scope.$apply();
					if (vm.uploadedFileTotalNum == uploader.queue.length) {
						ToastrAlert.sendStatus("success", "全部文件上传成功", 1000);
					}
				}, function(err) {
					console.log(err);
				});
			}
		};

		vm.updateRemain = function(item) {
			uploader.queue.splice(uploader.queue.indexOf(item), 1);

			var notUploadItem = uploader.queue.filter(function(queueItem) {
				return queueItem.isReady !== true;
			});
			vm.uploadedFileTotalNum = uploader.queue.length - notUploadItem.length;

			uploader.progress = (vm.uploadedFileTotalNum / uploader.queue.length).toFixed(2) * 100;
		};

		vm.uploadRemaining = function() {
			uploader.queue.forEach(function(queueItem) {
				vm.uploadFileItem(queueItem);
			});
		};

		vm.isUploadAllButtonEnabled = function() {
			if (uploader.queue.length <= vm.uploadedFileTotalNum) {
				var notUploadItem = uploader.queue.filter(function(queueItem) {
					return queueItem.isReady !== true;
				});
				if (notUploadItem.length > 0) {
					vm.uploadedFileTotalNum = uploader.queue.length - notUploadItem.length;
					return true;
				}
			}

			if (uploader.queue.length > vm.uploadedFileTotalNum)
				return true;

			if (uploader.queue.length === 0)
				return false;
		};

		uploader.onAfterAddingFile = function(fileItem) {};

		uploader.clearQueue = function() {
			uploader.queue = [];
			vm.remainFileNum = 0;
		};
	}
})();