(function() {
	"use strict";

	angular.module('wxWeb').controller('SelfIntroController', ['User', 'SelfIntroService', 'ToastrAlert', SelfIntroController]);

	function SelfIntroController(User, SelfIntroService, ToastrAlert) {
		var vm = this;

		vm.intro = '';

		vm.getIntro = function(wxid) {
			SelfIntroService.getIntro().then(function(response) {
				vm.intro = response.data.data.content;
			}, function(error) {});
		};

		vm.setIntro = function() {
			SelfIntroService.setIntro(vm.intro).then(function(response) {
				ToastrAlert.sendStatus("success", "自我介绍修改成功！", 1000);
			}, function(error) {});
		};

		vm.getIntro(User.userId);
	}
})();