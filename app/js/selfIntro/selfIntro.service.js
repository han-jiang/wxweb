(function() {
	"use strict";

	angular.module('wxWeb').service('SelfIntroService', ['$http', SelfIntroService]);

	function SelfIntroService($http) {
		var svc = this;

		svc.getIntro = function() {
			return $http({
				method: 'GET',
				url: base_url + '/json/dalao/intro.json',
				withCredentials: true
			});
		};

		svc.setIntro = function(introData) {
			return $http({
				method: 'POST',
				url: base_url + '/json/dalao/intro.json',
				data: {
					'content': introData
				},
				withCredentials: true
			});
		};
	}
})();