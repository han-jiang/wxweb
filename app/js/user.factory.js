(function() {
	"use strict";

	angular.module('wxWeb').factory('User', ['$cookies', UserFactory]);

	function UserFactory($cookies) {
		var userService = {};

		userService.userId = $cookies.get("userId");
		userService.currentDeviceId = $cookies.get("deviceId") || null;
		userService.deviceName = $cookies.get("deviceName");
		userService.isDebug = false;

		userService.chooseDevice = function(id) {
			if (userService.userId == "ventunity") {
				//管理员任意控制
				userService.currentDeviceId = id;
				$cookies.put("deviceId", id);
			} else if (userService.userId == "dalaozhaopin") {
				//大佬招聘只能控制自己的账号
				userService.currentDeviceId = "wxid_mjwgzjutap6912";
				$cookies.put("deviceId", "wxid_mjwgzjutap6912");
			} else {
				//普通用户只能为保命小号
				userService.currentDeviceId = "wxid_9d4kc1wwv9ms22";
				$cookies.put("deviceId", "wxid_9d4kc1wwv9ms22");
			}

			userService.deviceName = getDeviceName(id);
			$cookies.put("deviceName", userService.deviceName);
		};

		userService.isLogin = function() {
			return (userService.userId != null || userService.currentDeviceId != null);
		};

		userService.setUserId = function(id) {
			if (id !== null) {
				if (id == "ventunity") {
					userService.isDebug = true;
				} else {
					userService.isDebug = false;
				}
				userService.userId = id;
				$cookies.put("userId", id);
			}
		};

		userService.clearUserIdAndDevice = function() {
			if (userService.userId) {
				userService.userId = undefined;
				$cookies.remove("userId");
			}
			if (userService.currentDeviceId) {
				userService.currentDeviceId = undefined;
				$cookies.remove("deviceId");
				$cookies.remove("deviceName");
			}
		};

		function getDeviceName(id) {
			switch (id) {
				case "wxid_mjwgzjutap6912":
					return "大佬招聘";
				case "wxid_8bdf6sw9ql4a22":
					return "江瀚小号";
				case "jianghan9013":
					return "江瀚号";
				case "wxid_059t2c4agxk312":
					return "大猫 Meo";
				case "wxid_6hhapzp1zwpw22":
					return "万享小猫";
				case "wxid_9d4kc1wwv9ms22":
					return "保命小号";
			}
		}

		return userService;
	}
})();