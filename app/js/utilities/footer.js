$(function () {
	function setFooterPosition() {
		if ($('body').height() + 100 < $(window).height()) {
			$('footer').css({
				'position': 'fixed',
				'left': '0',
				'right': '0',
				'bottom': '0'
			});
		} else {
			$('footer').css({
				'position': 'static'
			});
		}
	}

	setFooterPosition();

	function setBodyBorder() {
		if ($(".main").height() < $(".sidebar").height()) {
			$(".main").css({
				"border-left": "none"
			});
			$(".sidebar").css({
				"border-right": "1px solid #E7E7EB"
			});
		} else {
			$(".sidebar").css({
				"border-right": "none"
			});
			$(".main").css({
				"border-left": "1px solid #E7E7EB"
			});
		}
	}

	setBodyBorder();

	//还没找到好的办法，暂时使用setInterval
	setInterval(function () {
		setFooterPosition();
		setBodyBorder();
	}, 500);
});
