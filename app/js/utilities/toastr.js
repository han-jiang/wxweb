(function() {
	"use strict";
	angular.module('wxWeb').factory('ToastrAlert', ['$cookieStore', 'toastr', ToastrAlertFactory]);

	function ToastrAlertFactory($cookieStore, toastr) {
		var Toastr = {};

		Toastr.sendStatus = function(status, message, title, timeout) {
			if (typeof title == "number") {
				timeout = title;
				if (status == "success") {
					toastr.success(message, {
						timeOut: timeout
					});
				} else if (status == "info") {
					toastr.info(message, {
						timeOut: timeout
					});
				} else if (status == "warning") {
					toastr.warning(message, {
						timeOut: timeout
					});
				} else if (status == "error") {
					toastr.error(message, {
						timeOut: timeout
					});
				}
			} else if (typeof title == "string") {
				if (status == "success") {
					toastr.success(message, title, {
						timeOut: timeout
					});
				} else if (status == "info") {
					toastr.info(message, title, {
						timeOut: timeout
					});
				} else if (status == "warning") {
					toastr.warning(message, title, {
						timeOut: timeout
					});
				} else if (status == "error") {
					toastr.error(message, title, {
						timeOut: timeout
					});
				}
			}
		};

		return Toastr;
	}
})();