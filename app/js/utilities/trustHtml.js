(function() {
	'use strict';
	angular.module('wxWeb').filter('trustHtml', function($sce) {
		return function(input) {
			return $sce.trustAsHtml(input);
		};
	});
})();