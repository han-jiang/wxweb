(function() {
	"use strict";
	angular.module('wxWeb').controller('wxWebController', ['$scope', '$location', 'User', 'LoginService', 'ToastrAlert', wxWebController]);

	function wxWebController($scope, $location, User, LoginService, ToastrAlert) {
		var vm = this;

		vm.user = User;

		vm.isUserLogin = function() {
			return User.isLogin();
		};

		vm.logout = function() {
			LoginService.logout().then(function(res) {
				User.clearUserIdAndDevice();
				$location.path("/about");
				ToastrAlert.sendStatus("success", "成功退出登陆", 1000);
			}, function(err) {
				ToastrAlert.sendStatus("error", "请检查网络连接", "登出失败", 1000);
			});
		};
	}
})();