### 1.grunt 
a. 打包：`grunt` (发布时需要跑一遍`grunt`来更新dist目录下的文件)
	
- 首次会打包依赖到`app/dist/dependencies.min.js`
- 之后监听文件变化并重新打包项目js到`app/dist/main.js 和 main.min.js`
- 样式文件到`app/dist/style.min.css`

b. 只更新打包项目文件：`grunt dist`

c. 测试：`grunt test`自动监听文件变化并重跑测试