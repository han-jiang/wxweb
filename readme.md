wxWeb 微信管理平台

这是一个纯静态站点(HTML/CSS/JavaScript)。使用 angularjs，bootstrap 

文件结构

```
-app/
	--/api
	--/css
	--/dist
	--/fonts
	--/img
	--/js
	app.js
	index.html
doc/
node_modules/
.eslintrc.js
.gitignore
Gruntfile.js
karma.conf.js
package.json
readme.md
```

* 使用 angular.js 开发
* 使用了 Leancloud 进行文件和数据存储
* 使用 Wilddog 进行小数据的存储
* 使用 grunt 打包
* 使用 karma 测试框架

后端 API 使用 Python 开发

API 会请求 MQTT 服务器

MQTT 服务器会请求设备

设备请求微信，微信读取数据库放回说需要的信息